package ch.memobase

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MessagesSpec extends AnyFunSuite with Matchers {
  test("a syntactically sound termination message should match") {
    val processId = "proc1"
    val jobName = "test-job"

    ShutdownMessage(processId, jobName).compare(
      s"""{"action": "termination", "processId": "$processId", "job-name":"$jobName"}"""
    ) shouldBe true
  }
}
