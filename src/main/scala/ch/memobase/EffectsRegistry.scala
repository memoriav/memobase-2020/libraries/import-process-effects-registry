/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase

import java.util.Properties
import java.time.Duration
import scala.collection.mutable.HashMap
import scala.collection.JavaConverters._

import org.apache.kafka.streams.{StreamsBuilder => JavaStreamsBuilder}
import org.apache.kafka.streams.scala.{StreamsBuilder => ScalaStreamsBuilder}
import org.apache.logging.log4j.scala.Logging

class EffectsRegistry() extends Logging {
  import EffectsRegistry._
  private val registry = HashMap[Message, Effect]()

  def register(message: Message, effect: Effect): EffectsRegistry = {
    logger.debug(s"Register new effect for message: $message")
    registry += ((message, effect))
    this
  }

  def run(builder: ScalaStreamsBuilder, inputTopic: String): Unit = {
    import org.apache.kafka.streams.scala.ImplicitConversions._
    import org.apache.kafka.streams.scala._
    import Serdes._

    builder
      .stream[String, String](inputTopic)
      .foreach((_, v) => performEffect(v))
  }

  def run(builder: JavaStreamsBuilder, inputTopic: String): Unit = {
    builder
      .stream[String, String](inputTopic)
      .foreach((_, v) => performEffect(v))
  }

  private def performEffect(value: String): Unit = {
    registry
      .collectFirst {
        case e if e._1.compare(value) => e._2
      }
      .foreach(effect => {
        logger.debug(
          s"Performing effect ${effect.name} ${effect.description
            .foreach("(" + _ + ")")}"
        )
        effect.action()
      })
  }
}

object EffectsRegistry {
  def apply(): EffectsRegistry = new EffectsRegistry()
}
