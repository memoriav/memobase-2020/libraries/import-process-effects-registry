# Import Process Effects Registry

In its core, this library maps maintenance messages targeted at individual
services inside the import workflows to effects which should be applied on
receiving such messages. For that purpose it provides a `EffectsRegistry`
class. This utility class helps to register message patterns matching to a
certain effect. Messages have a `compare` method which is used to compare the
message pattern with the message currently processed by the application.
`Effect` is a case class requiring a name, an action which is performed upon a
message match, and an optional description. 

## Usage

```scala
import ch.memobase.{EffectsRegistry, Effect, ShutdownMessage}

val builder = ... // Set up KafkaStream
val inputTopic = ...

// `inputTopic` is the name of the topic where the maintenance message are
stored; props is a variable containing properties for the Kafka consumer
val registry = EffectsRegistry()
val shutdownEffect = Effect("shutdown", () => sys.exit(0), Some("shuts down
application"))
// the `register` method takes a message matcher as well as an instance of the
`Effect` case class
registry.register(ShutdownMessage, shutdownEffect)
// start to consume messages
registry.run(builder, inputTopic)

```
