import Dependencies._

ThisBuild / scalaVersion := "2.12.12"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / startYear := Some(2020)
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Import Process Effects Registry",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    bintrayOrganization := Some("memoriav"),
    bintrayRepository := "memobase",
    bintrayVersionAttributes := Map(),
    bintrayVcsUrl := Some(
      "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/import-process-effects-registry"
    ),
    git.baseVersion := "0.1.0",
    git.useGitDescribe := true,
    libraryDependencies ++= Seq(
      kafkaStreams % "provided",
      log4jApi,
      log4jScala,
      uPickle,
      scalaTest % Test
    ),
    licenses += ("Apache-2.0", new URL(
      "https://www.apache.org/licenses/LICENSE-2.0.txt"
    ))
  )
